## YouTube

This is a public repo used to store code from my YouTube videos.


## Setting Up GitLab Runner

So first you need to install a runner. You can do this locally depending on your operating system. This example will focus on debian based systems for 2 reasons. The first is that, it's the type of distro I am using. The second is that it's easier to install tools and things like this on a linux based system as opposed to something like windows.

##### Install the runner
1. Download the deb package:
```curl -LJO "https://s3.dualstack.us-east-1.amazonaws.com/gitlab-runner-downloads/latest/deb/gitlab-runner_amd64.deb"```

2. Install the deb package:
```dpkg -i gitlab-runner_amd64.deb```

##### Register the runner
1. Register the runner:
```sudo gitlab-runner register```

2. Enter the gitlab url:
```https://gitlab.example.com```

3. Get a registration token from the project you want to set this up on. PROJECT_NAME > Settings > CI/CD > Click the 3 dots beside `New Project Runner`, copy that token and enter that in the token section

4. Enter a runner name

5. Leave tags section blank, these can be updated on gitlab.

6. Leave maintenance note blank.

7. Enter the runner executor. We are using `shell` in this example so type that in.


##### Start the runner
To start the runner type, `sudo gitlab-runner start` in the terminal and hit enter
